<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }



    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $colors = json_encode([
            [
                "name" => "Acid Green",
                "hex" => "#B0BF1A"
            ],
            [
                "name" => "Aero",
                "hex" => "#7CB9E8"
            ],[
                "name" => "African Violet",
                "hex" => "B284BE"
            ]
        ]);
        return view('home', compact('colors'));
    }

    public function dashboard()
    {
        return view('dashboard');
    }

    public function valueChain($crop)
    {
        return view('valueChain', compact('crop'));
    }

    public function regions($region)
    {
        return view('regions', compact('region'));
    }

    public function documents()
    {
        return view('documents');
    }
}
