<?php

use Illuminate\Http\Request;

function formatRegionName($region)
{
    return ucfirst(str_replace("_", " ", $region));
}

function isLinkActive($link, $class = "active")
{

    if(url()->current() == route("/") && $link == "home") {
        return $class;
    }else if(strpos(url()->current(), $link) !== false) {
        return $class;
    }
    return "";
}
