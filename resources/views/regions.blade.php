@extends('layouts.app')

@section('content')
    <region-component regionvalue="{{$region}}" regionname="{{formatRegionName($region)}}"></region-component>
@stop
