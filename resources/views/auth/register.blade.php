@extends('layouts.auth.app')

@section('content')
    <div class="authentication">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-12">
                    <form method="POST" action="{{route('register')}}" class="card auth_form">
                        @csrf
                        <div class="header">
                            <img class="logo" src="assets/images/profile_av.jpg" alt="">
                            <h5>Sign Up</h5>
                            <span>Register a new membership</span>
                        </div>
                        <div class="body">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="username" name="name" required>
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="zmdi zmdi-account-circle"></i></span>
                                </div>
                            </div>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" name="email" placeholder="Enter Email" required>
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="zmdi zmdi-email"></i></span>
                                </div>
                            </div>
                            <div class="input-group mb-3">
                                <input type="text" name="password" class="form-control" placeholder="Password" required>
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="zmdi zmdi-lock"></i></span>
                                </div>
                            </div>
                            <div class="checkbox">
                                <input id="remember_me" type="checkbox">
                                <label for="remember_me">I read and agree to the <a href="javascript:void(0);">terms of usage</a></label>
                            </div>
                            <button  type="submit" class="btn btn-primary btn-block waves-effect waves-light">SIGN UP</button>
                            <div class="signin_with mt-3">
                                <a class="link" href="{{route('login')}}">You already have a membership?</a>
                            </div>
                        </div>
                    </form>
                    <div class="copyright text-center">
                        &copy;{{date('Y')}}
                        <span><a href="javascript:void(0);">ABDC</a></span>
                    </div>
                </div>
                <div class="col-lg-8 col-sm-12">
                    <div class="card">
                        <img src="assets/images/coffee.jpg" alt="Sign Up" />
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
