@extends('layouts.auth.app')

@section('content')

    <div class="authentication">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-12">
                    <form method="POST" action="{{route('login')}}" class="card auth_form">
                        @csrf
                        <div class="header">
                            <img class="logo" src="assets/images/profile_av.jpg" alt="">
                            <h5>Log in</h5>
                        </div>
                        <div class="body">
                            @if ($errors->has('email'))
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                            @endif

                            <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="email" name="email">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="zmdi zmdi-account-circle"></i></span>
                                </div>
                            </div>
                            <div class="input-group mb-3">
                                <input type="password" class="form-control" placeholder="Password" name="password">
                                <div class="input-group-append">
                                    <span class="input-group-text"><a href="forgot-password.html" class="forgot" title="Forgot Password"><i class="zmdi zmdi-lock"></i></a></span>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block waves-effect waves-light">
                                SIGN IN
                            </button>

                        <div class="signin_with mt-3">
                                <a class="link" href="{{route('register')}}">I have no account.</a>
                            </div>
                        </div>
                    </form>
                    <div class="copyright text-center">
                        &copy;
                        <script>document.write(new Date().getFullYear())</script>,
                        <span><a href="">ABCD Portal</a></span>
                    </div>
                </div>
                <div class="col-lg-8 col-sm-12">
                    <div class="card">
                        <img src="assets/images/coffee.jpg" lt="Sign In"/>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
