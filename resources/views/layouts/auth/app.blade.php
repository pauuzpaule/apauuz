<!doctype html>
<html class="no-js " lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <title>ABCD Value Chain Portal: Auth</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}"><!-- Favicon-->

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">



</head>

<body class="theme-blush">
<div id="app">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="m-t-30"><img class="zmdi-hc-spin" src="{{asset('assets/images/loader.svg')}}" width="48" height="48" alt="Aero"></div>
            <p>Please wait...</p>
        </div>
    </div>

    <!-- Main Content -->
    @yield('content')

</div>

<script src="{{ asset('js/app.js') }}" defer></script>
<script src="{{ asset('js/pages/c3.bundle.js') }}" defer></script>
<script src="{{ asset('js/pages/index.js') }}" defer></script>

</body>


</html>
