<!doctype html>
<html class="no-js " lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <title>ABCD Value Chain Portal: Home</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon"> <!-- Favicon-->

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">



</head>

<body class="theme-blush">
<div id="app">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="m-t-30"><img class="zmdi-hc-spin" src="{{asset('assets/images/loader.svg')}}" width="48" height="48" alt="Aero"></div>
            <p>Please wait...</p>
        </div>
    </div>

    <!-- Overlay For Sidebars -->
    @include('layouts.overlaySideBar')

    <!-- Main Search -->
    @include('layouts.searchBar')

    <!-- Right Icon menu Sidebar -->
    @include('layouts.iconMenuRight')

    <!-- Left Sidebar -->
   @include('layouts.navLeft')

    <!-- Right Sidebar -->
    @include('layouts.navRight')

    <!-- Main Content -->

    <section class="content {{url()->current() == route('documents')? 'file_manager': ''}}">
        @yield('content')
    </section>

</div>

<!-- Scripts -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCBE7aRb0VJdRBlUnvjcQ_lnKWoAWuXUx8&libraries=places,geometry"></script>
<script src="{{ asset('js/app.js') }}" defer></script>
<script src="{{ asset('js/pages/c3.bundle.js') }}" defer></script>
<script src="{{ asset('js/pages/index.js') }}" defer></script>

</body>


</html>
