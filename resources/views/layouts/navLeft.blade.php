<aside id="leftsidebar" class="sidebar">
    <div class="navbar-brand">
        <button class="btn-menu ls-toggle-btn" type="button"><i class="zmdi zmdi-menu"></i></button>
        <a href="{{route('/')}}"><img src="{{asset('assets/images/logo.svg')}}" width="25" alt="Aero"><span class="m-l-10">ABCD Value Chain</span></a>
    </div>
    <div class="menu">
        <ul class="list">
            <li>
                <div class="user-info">
                    <a class="image" href=""><img src="{{asset('assets/images/profile_av.jpg')}}" alt="User"></a>
                    <div class="detail">
                        <h4>{{auth()->check() ? auth()->user()->name : ""}}</h4>
                        <small>Admin</small>
                    </div>
                </div>
            </li>
            <li class="{{isLinkActive('home')}} open"><a href="{{route('/')}}"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>


            <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-alt"></i><span>Notifications</span></a>
                <ul class="ml-menu">
                </ul>
            </li>

            <li class="{{isLinkActive("value-chain")}}"> <a href="{{route('value-chain', ['crop' => 'coffee'])}}"><i class="zmdi zmdi-local-florist"></i><span>Value Chains</span></a>
<!--                <ul class="ml-menu">
                    <li><a href="{{route('value-chain', ['crop' => 'coffee'])}}">Coffee</a></li>
                    <li><a href="{{route('value-chain', ['crop' => 'tea'])}}">Tea</a></li>
                    <li><a href="{{route('value-chain', ['crop' => 'rice'])}}">Rice</a></li>
                    <li><a href="{{route('value-chain', ['crop' => 'cotton'])}}">Cotton</a></li>
                    <li><a href="{{route('value-chain', ['crop' => 'maize'])}}">Maize</a></li>
                    <li><a href="{{route('value-chain', ['crop' => 'cocoa'])}}">Cocoa</a></li>
                    <li><a href="{{route('value-chain', ['crop' => 'beans'])}}">Beans</a></li>

                </ul>-->
            </li>

            <li class="{{isLinkActive("regions")}}"><a href="{{route('region', ['region' => 'west_nile'])}}"><i class="zmdi zmdi-map"></i><span>Regions</span></a>

<!--                <ul class="ml-menu">
                    <li><a href="{{route('region', ['region' => 'west_nile'])}}">West Nile</a></li>
                    <li><a href="{{route('region', ['region' => 'karamoja'])}}">Karamoja</a></li>
                    <li><a href="{{route('region', ['region' => 'lango'])}}">Lango</a></li>
                    <li><a href="{{route('region', ['region' => 'bukedi'])}}">Bukedi</a></li>
                    <li><a href="{{route('region', ['region' => 'teso'])}}">Teso</a></li>
                    <li><a href="{{route('region', ['region' => 'acholi'])}}">Acholi</a></li>
                    <li><a href="{{route('region', ['region' => 'elgon'])}}">Elgon</a></li>
                    <li><a href="{{route('region', ['region' => 'busoga'])}}">Busoga</a></li>
                    <li><a href="{{route('region', ['region' => 'bunyoro'])}}">Bunyoro</a></li>
                    <li><a href="{{route('region', ['region' => 'toro'])}}">Toro</a></li>
                    <li><a href="{{route('region', ['region' => 'north_buganda'])}}">North Buganda</a></li>
                </ul>-->
            </li>

            <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-accounts-alt"></i><span>Stakeholders</span></a>
                <ul class="ml-menu">
                </ul>
            </li>

            <li class="{{isLinkActive("regions")}}"><a href="{{route('documents')}}" ><i class="zmdi zmdi-folder"></i><span>Reference Docs</span></a>
            </li>

            <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-lock"></i><span>User Accounts</span></a>

            </li>


        </ul>
    </div>
</aside>
