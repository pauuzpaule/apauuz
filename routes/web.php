<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('/');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'dashboard'])->name('dashboard');
Route::get('/value-chain/{crop?}', [App\Http\Controllers\HomeController::class, 'valueChain'])->name('value-chain');
Route::get('/regions/{region?}', [App\Http\Controllers\HomeController::class, 'regions'])->name('region');
Route::get('/documents', [App\Http\Controllers\HomeController::class, 'documents'])->name('documents');
